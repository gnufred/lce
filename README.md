# Les Chroniques Entropiques

This repository store an RPG Maker project I started at college. I have created about 2 hours worth of playable top down RPG.

The main goal here is to keep of all the battle scripts and retro graphics I had to create to make the battle system work. There's 9 fully playable and balanced characters. Every characters has custom made graphics, skills, sounds, stats and scripts. Each character has custom play style which was coded with RPGMaker scripts over what the base game offer.

This game has never been finished. Turns out creating an RPG of Final Fantasy 6 magnitude is above the time a single person at college can spend on game making :)

The interesting stuff is in: `/CharSet`, `/Battle`, `/BattleCharSet` and `/BattleWeapon`.
