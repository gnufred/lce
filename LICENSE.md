# License

All RPG Maker files belongs to their rightfull original owner, under whichever license they are under.

Scripts, media and content created by me are distributed under under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).
